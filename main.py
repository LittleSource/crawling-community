from xlrd import open_workbook
from xlutils.copy import copy
import requests
import re
import time
#自动爬取贝壳小区数据
#调用腾讯地图API获取小区poi等数据
#写入excel表格，使用前请创建bc.xls表格
#作者 LittleSource
url = "https://tj.ke.com/xiaoqu/"
header = {'user-agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.121 Safari/537.36'}
params = "zhongxinguangchang"
mapUrl = "https://apis.map.qq.com/ws/place/v1/search"
mapParams = {'boundary':'region(天津市,0)','key':'7YDBZ-4ATCD-5GM4Z-HCI5B-4ECM6-PPBXO','filter':'category=房产小区'}
pageSum = 2
r_xls = open_workbook("bc.xls")
row = r_xls.sheets()[0].nrows
excel = copy(r_xls)
worksheet = excel.get_sheet(0)
nowTime = int(time.time())
print(params)
for i in range(1,pageSum+1):
    res = requests.get(url+params+'/pg'+str(i),headers=header)
    html = res.text.encode('utf-8')
    items = re.findall('.*data-maidian=".*?" title=".*?">(.*)</a>.*',html.decode())
    for data in items:
        time.sleep(0.3)
        mapParams['keyword'] = data
        mapRes = requests.get(mapUrl,params=mapParams,headers=header)
        mapDict = mapRes.json()
        if mapDict["status"] == 0 and len(mapDict["data"]) > 0:
            print(mapDict["data"][0]['title'])
            worksheet.write(row,2, mapDict["data"][0]['ad_info']['province'])
            worksheet.write(row,3, mapDict["data"][0]['ad_info']['city'])
            worksheet.write(row,4, mapDict["data"][0]['ad_info']['district'])
            worksheet.write(row,5, mapDict["data"][0]['title'])
            worksheet.write(row,6, mapDict["data"][0]['id'])
            worksheet.write(row,7, nowTime)
            worksheet.write(row,8, nowTime)
            row=row + 1
        else:
            print(mapDict["message"])
excel.save('bc.xls')